# Objectifs

Notre groupe de travail se donne pour mission de participer par tous les
moyens à la mise en place et la promotion d'outils culturels,
organisationnels et techniques pour l'émergence d'une informatique
résiliente, sobre et que nous pensons post-personnelle.

Ce changement fondamental de la culture numérique ne nous semble pas être un
choix mais une une conséquence inévitable de la dynamique actuelle:
que nous le voulions ou non, l'informatique se réduira à sa plus simple
expression lorsque nous n'aurons plus les moyens de maintenir des systèmes
plus consommateurs en ressources.

Nous sommes conscients que nos objectifs ne sont pas en adéquation avec le
modèle économique actuel. Nous percevons notre travail comme partie d'un
mouvement général de refonte de la société.

Tenter d'initier la sobriété numérique des maintenant, c'est:

* s'inscrire dans la lutte contre de nombreuses formes de polutitions
  dont la plus grave a pour effet le changement climatique.
* économiser des ressources qui seront peut-être vitales pour
  les générations futures.
* préparer les acteurs du numérique à maintenir et utiliser des
  infrastructures qui prendront, nous l'espérons, le relais des
  systèmes qui ont fait la preuve de leur utilité dans l'organisation
  collective ainsi que dans la production et la diffusion du savoir.

Il nous faut des aujourd'hui produire les architectures et les compétences
nécessaires au maintient des usages numériques essentiels. Nous nous fixons
donc

* d'identifier ces services essentielles et d'en proposer des versions
  fonctionnelles dans des environnements techniques fortement contraints.
* de promouvoir ces outils comme des moyens
  * de préparation à la résilience
  * de diminution de notre impact numérique
* de mettre en oeuvre les conditions de la transmission d'une expertise
  suffisante au sein de la population et d'accompagnement des usagers les
  moins favorisés. Nous pensons que cette transmission passe une éducation
  nationale (ou populaire en cas de défaillance de l'institution): la
  connaissance grands principes de base du numérique est aussi
  importante que l'acquisition d'une langue vivante et des rudiments
  de mathématiques.
* cette augmentation du niveau de technicité de la population est rendu
  nécessaire par ailleurs: elle est un outils de citoyenneté dans le
  monde ultra-connecté dans lequel nous vivons encore.

# Stratégie

le courant minimaliste a constitué un ensemble d'outils culturels et
techniques pour une informatique:

* qui mobilise assez peu de concepts et d'outils pour permettre la
  transmission de l'expertise dans des temps raisonnables.
* qui permette une maintenance par une large population.
* qui est probablement la plus efficiente énergétiquement parlant
  (reste à mesurer)
* qui est la seule à l'heure actuelle à pouvoir être exécutée sur des
  solutions techniques contraintes.

Nous nous fixons pour missions premières:

* la sensibilisation aux enjeux et la nécessité de l'adoption d'une
  informatique minimaliste.
* la réflexion des solutions alternatives aux usages actuels (nouveaux canaux
  pour la video, retour à des formats analogiques, licence globale, ...). La
  production de données sur les impacts de ces alternatives.
* la réflexion sur les évolutions possibles de plateformes logicielles.
* la réflexion et les expérimentations sur la promotion et l'enseignement
  d'une culture numérique minimaliste.

Il est toutefois exclus de penser que la prise de conscience va être massive
et rapide. La participation à la promotion de bonnes pratiques considérant
l'état actuel de l'informatique est une autre mission.

# Fonctionnement du groupe

Le fonctionnement de notre groupe doit permette d'expérimenter les outils
organisationnels et techniques que nous validons. Entre autre, cela permet à
chacun d'entre nous de pouvoir produire des retours d'expérience concrets à nos
interlocuteurs. Dans ce but:

* l'intégralité de la production intellectuelle de la communauté sera
  disponible librement et gratuitement sur des plateformes distribuées.
  les miroirs seront encouragés.
* la participation à nos travaux est ouverte à tous et encouragée. Nous n'aurons
  recours à la modération que si des pratiques constituent une entrave signalée
  et débattue publiquement à la bonne marche du groupe.
* l'assistance des nouveaux collaborateurs est une des missions que nous nous
  fixons.
* si nous invitons toute personne à travailler avec nous, les outils que nous
  utilisons pour la production documentaire et les échanges doivent pouvoir être
  utilisés par les outils minimalistes.
* les plateformes utilisées doivent permettre une réplication des données sur
  des instances tiers.

Ainsi, nous tentons de produire une stratégie à long terme d'accompagnement
d'acteurs numériques responsables et capables d'un numérique que nous espérons
libre, solidaire et équitable en plus d'être sobre et résilient.

# repenser la structure sociale

* penser l'utilisateur comme un élément actif et responsable
  de la maintenance des systèmes.
* valoriser la notion de transmission d'expertise dans les
  métiers du support.
* nous donner la capacité d'investir dans des solutions techniquement
  et écologiquement pertinentes sans contraintes commerciales.
* nous donner la capacité de proposer aux organismes des répartitions du
  travail qui permette l'émergence de fonctionements adéquats
  (TODO: décrire le libre et le tool smithing)

